import React from 'react';
import './index.css';
import {  Button, Container, Row, Col, Image, ListGroup } from 'react-bootstrap';
const accounting = require('accounting');

class Index extends React.Component {
  constructor(props){
    super(props);
    this.state = {
    }
  }
  render(){
    const { data } = this.props;
    return (
      <ListGroup.Item>
        <Row className="align-baseline">
          <Col md={3} align="right">
          <i>{accounting.formatMoney(Math.abs(data.amount), '₹', 0)}</i>
          </Col>
          <Col md={9}>
            <Row>
              <b>{data.detail}</b>
            </Row>
            <Row>
              {}
            </Row>
          </Col>
        </Row>
      </ListGroup.Item>
    );
  }
}

export default Index;
