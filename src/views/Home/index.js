import React from 'react';
//import logo from '../../images/logo.svg';
import './index.css';
import { withTranslation } from 'react-i18next';
import {  Button, Container, Row, Col, Image, Jumbotron, Carousel, Dropdown, ListGroup } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import Login from '../../components/Login';
import DatePicker from 'react-date-picker';
import api from '../../api';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCoffee, faArrowCircleDown, faArrowCircleUp } from '@fortawesome/free-solid-svg-icons'
import Txn from './Txn';
const accounting = require('accounting');

var moment = require('moment');
const { client } = api;

class Index extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      name: "Vinay Nbd",
      balance: 100,
      date: new Date()
    }
  }

  componentDidMount(){
    //console.log(client.getData());
  }

  componentWillUnmount(){

  }

  setDate = (event, date) => {
    //date = date || this.state.date;

    this.setState({
      //show: false,
      date
    });

    /*if (date != undefined) {
      this.updateTxns();
    }*/
  }

  render(){
    const {t, i18n} = this.props;
    const { name, balance, date } = this.state;
    const txns = [
      {amount: 1000000, account: "Abhijeet Goel", detail: "hello", created_at: new Date(), updated_at: new Date()},
      {amount: 200, account: "Abhijeet Goel", detail: "hello", created_at: new Date(), updated_at: new Date(), cancelled: true},
      {amount: 100, detail: "hello", created_at: new Date(), updated_at: new Date()}
    ];
    return (
      <Container className="">
        <Row align="center">
          <Col md={12}>
            <h2>{name}</h2>
          </Col>
        </Row>
        <hr />
        <Row>
          <Col md={6}>
            <ListGroup className="">
              <ListGroup.Item className="header">
                <Row className="align-baseline header">
                  <Col md={12} align="center">
                    <FontAwesomeIcon icon={faArrowCircleDown} size="1x" /> <b>You Receive</b>
                  </Col>
                </Row>
              </ListGroup.Item>
              {txns.map((txn, i) => <Txn data={txn} />)}
            </ListGroup>
          </Col>
          <Col md={6}>
            <ListGroup.Item>
              <Row className="align-baseline">
                <Col md={12} align="center">
                  <FontAwesomeIcon icon={faArrowCircleUp} size="1x" /> <b>You Gave</b>
                </Col>
              </Row>
            </ListGroup.Item>
            <ListGroup>
              {txns.map((txn, i) => <Txn data={txn} />)}
            </ListGroup>
          </Col>
        </Row>
        <hr />
        <Row align="center">
          <Col md={12}>
            <b>Balance</b> : {accounting.formatMoney(Math.abs(balance), '₹', 0)}
          </Col>
        </Row>
      </Container>
    );
  }
}

export default withTranslation()(Index);
